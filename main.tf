terraform {
  required_providers {
    helm = {
      source = "hashicorp/helm" 
      version = "2.0.3" #minimum version 2.0.3 of provider works, older versions fail
    }
  }
}

provider "helm" {
kubernetes {
  host                   = "https://{{HOST IP}}" # eg Microk8s = https://{{cluster-ip}}:16443"
  insecure = true #should be false in prod...
  token = "{{TOKEN}}" #get from .kube/config or microk8s config
    }
}

resource "helm_release" "otel" {
  name       = "splunk-otel"
  repository = "https://signalfx.github.io/splunk-otel-collector-chart"
  chart      = "splunk-otel-collector"
  version = "{{VERSION}}"

  set {
    name  = "clusterName"
    value = "{{CLUSTER NAME}}"
  }
  set {
    name  = "splunkObservability.realm"
    value = "{{OBSERVABILITY REALM}}" #au0
  }
  set {
    name  = "splunkObservability.accessToken"
    value = "OBSERVABILITY ACCESS TOKEN"
  }
  set {
    name  = "splunkPlatform.endpoint"
    value = "https://{{SPLUNK HEC URL}}:8088/services/collector/event"
  }
  set {
    name  = "splunkPlatform.token"
    value = "{{YOUR HEC TOKEN}}"
  }
}
